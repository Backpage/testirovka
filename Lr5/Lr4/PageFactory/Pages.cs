﻿using Lr5.PageObjects;

namespace Lr5.PageFactory
{
    public static class Pages
    {
        public static PetTypesPageObject PetTypesPage => new PetTypesPageObject(BaseTest.driver);
        public static OwnersPageObject OwnersPage => new OwnersPageObject(BaseTest.driver);
    }
}
