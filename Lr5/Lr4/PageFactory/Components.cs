﻿using Lr5.PageComponents;

namespace Lr5.PageFactory
{
    public static class Components
    {
        public static TabComponent Tab => new TabComponent(BaseTest.driver);
        public static TitleComponent Title => new TitleComponent(BaseTest.driver);
    }
}
