﻿using OpenQA.Selenium;

namespace Lr5.PageComponents
{
    public class TabComponent
    {
        private IWebDriver driver;

        public TabComponent(IWebDriver driver) => this.driver = driver;

        public IWebElement OwnerTab() => driver.FindElement(By.CssSelector(".ownerTab"));
        public IWebElement OwnerAllTab() => driver.FindElement(By.CssSelector(".open li:nth-child(1) > a"));
        public IWebElement OwnerAddTab() => driver.FindElement(By.CssSelector(".open li:nth-child(2) span:nth-child(2)"));
        public IWebElement PetTypeTab() => driver.FindElement(By.CssSelector("li:nth-child(4) span:nth-child(2)"));

        public void OwnersOpenAll()
        {
            OwnerTab().Click();
            OwnerAllTab().Click();
        }

        public void OwnersOpenAdd()
        {
            OwnerTab().Click();
            OwnerAddTab().Click();
        }

        public void PetTypesOpen()
        {
            PetTypeTab().Click();
        }
    }
}
