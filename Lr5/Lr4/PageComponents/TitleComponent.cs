﻿using OpenQA.Selenium;

namespace Lr5.PageComponents
{
    public class TitleComponent
    {
        private IWebDriver driver;

        public TitleComponent(IWebDriver driver) => this.driver = driver;

        public IWebElement Title() => driver.FindElement(By.CssSelector("h2"));

        public string GetTitleText()
        {
            return Title().Text;
        }
    }
}
