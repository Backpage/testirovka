﻿using OpenQA.Selenium;

namespace Lr5.PageObjects
{
    public class OwnersPageObject : BasePageObject
    {
        public OwnersPageObject(IWebDriver driver) : base(driver) { }

        private By OwnerLink = By.CssSelector("tr:last-child .ownerFullName > a");
        private By OwnerEdit = By.CssSelector(".editOwner");
        private By OwnerBack = By.CssSelector(".goBack");
        private By OwnerFirstName = By.Id("firstName");
        private By OwnerLastName = By.Id("lastName");
        private By OwnerAddress = By.Id("address");
        private By OwnerCity = By.Id("city");
        private By OwnerTelephone = By.Id("telephone");
        private By OwnerAdd = By.CssSelector(".addOwner");
        private By OwnerUpdate = By.CssSelector(".updateOwner");

        public void OpenInfo()
        {
            driver.FindElement(OwnerLink).Click();
        }

        public void OpenEdit()
        {
            driver.FindElement(OwnerEdit).Click();
        }

        public void Add(string firstName, string lastName, string address, string city, string telephone)
        {
            Helper.ClickAndSendKeys(driver.FindElement(OwnerFirstName), firstName);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerLastName), lastName);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerAddress), address);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerCity), city);
            Helper.ClickAndSendKeys(driver.FindElement(OwnerTelephone), telephone);  
            driver.FindElement(OwnerAdd).Click();
        }

        public string GetName()
        {
            return driver.FindElement(OwnerLink).Text;
        }

        public void Edit(string firstName)
        {
            Helper.ClickAndClearAndSendKeys(driver.FindElement(OwnerFirstName), firstName);
            driver.FindElement(OwnerUpdate).Click();
        }

        public void Back()
        {
            driver.FindElement(OwnerBack).Click();
        }
    }
}
