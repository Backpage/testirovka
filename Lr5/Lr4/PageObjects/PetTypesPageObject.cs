﻿using OpenQA.Selenium;

namespace Lr5.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        public PetTypesPageObject(IWebDriver driver) : base(driver) { }

        private By PetTypeAdd = By.CssSelector(".addPet");
        private By PetTypeName = By.Id("name");
        private By PetTypeSave = By.CssSelector(".saveType");
        private By PetTypeNameValue = By.CssSelector("tr:last-child input");
        private By PetTypeEdit = By.CssSelector("tr:last-child .editPet");
        private By PetTypeUpdate = By.CssSelector(".updatePetType");
        private By PetTypeDelete = By.CssSelector("tr:last-child .deletePet");        

        public void Add(string name)
        {
            driver.FindElement(PetTypeAdd).Click();
            Helper.ClickAndSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(PetTypeSave).Click();
        }

        public string GetName()
        {
            return driver.FindElement(PetTypeNameValue).GetAttribute("value");
        }

        public void Edit(string name)
        {
            driver.FindElement(PetTypeEdit).Click();
            Helper.ClickAndClearAndSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(PetTypeUpdate).Click();
        }

        public void Delete()
        {
            driver.FindElement(PetTypeDelete).Click();
        }
    }
}
