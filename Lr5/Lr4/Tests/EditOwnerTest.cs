using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class EditOwnerTest : BaseTest
    {
        [Test]
        public void EditOwner()
        {
            TabComponent tab = Components.Tab;
            OwnersPageObject ownersPage = Pages.OwnersPage;
            tab.OwnersOpenAll();
            Helper.Wait();
            ownersPage.OpenInfo();
            ownersPage.OpenEdit();
            ownersPage.Edit("Alex");
            Helper.Wait();
            ownersPage.Back();
            Helper.Wait();

            Assert.That(ownersPage.GetName(), Is.EqualTo("Alex Doe"));
        }
    }
}