using NUnit.Framework;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class OpenPetTypesPageTest : BaseTest
    {
        [Test]
        public void OpenPetTypesPage()
        {
            TabComponent tab = Components.Tab;
            TitleComponent title = Components.Title;
            tab.PetTypesOpen();
            Helper.Wait();

            Assert.That(title.GetTitleText(), Is.EqualTo("Pet Types"));
        }
    }
}