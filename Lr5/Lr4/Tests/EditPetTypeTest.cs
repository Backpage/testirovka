using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class EditPetTypeTest : BaseTest
    {
        [Test]
        public void EditPetType()
        {
            TabComponent tab = Components.Tab;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            tab.PetTypesOpen();
            Helper.Wait();
            petTypesPage.Edit("dog");
            Helper.Wait();

            Assert.That(petTypesPage.GetName(), Is.EqualTo("dog"));
        }
    }
}