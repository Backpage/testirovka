using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class AddOwnerTest : BaseTest
    {
        [Test]
        public void AddOwner()
        {
            TabComponent tab = Components.Tab;
            OwnersPageObject ownersPage = Pages.OwnersPage;
            tab.OwnersOpenAdd();
            ownersPage.Add("John", "Doe", "Peremohy str", "Zhytomyr", "0935678352");
            Helper.Wait();

            Assert.That(ownersPage.GetName(), Is.EqualTo("John Doe"));
        }
    }
}