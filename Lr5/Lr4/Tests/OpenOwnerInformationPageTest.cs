using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class OpenOwnerInformationPageTest : BaseTest
    {
        [Test]
        public void OpenOwnerInformationPage()
        {
            TabComponent tab = Components.Tab;
            TitleComponent title = Components.Title;
            OwnersPageObject ownersPage = Pages.OwnersPage;
            tab.OwnersOpenAll();
            Helper.Wait();
            ownersPage.OpenInfo();

            Assert.That(title.GetTitleText(), Is.EqualTo("Owner Information"));
        }
    }
}