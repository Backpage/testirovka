using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class DeletePetTypeTest : BaseTest
    {
        [Test]
        public void DeletePetType()
        {
            TabComponent tab = Components.Tab;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            tab.PetTypesOpen();
            Helper.Wait();
            string name = petTypesPage.GetName();
            petTypesPage.Delete();
            Helper.Wait();

            Assert.That(petTypesPage.GetName(), Is.Not.EqualTo(name));
        }
    }
}