using NUnit.Framework;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class OpenOwnerAddPageTest : BaseTest
    {
        [Test]
        public void OpenOwnerAddPage()
        {
            TabComponent tab = Components.Tab;
            TitleComponent title = Components.Title;
            tab.OwnersOpenAdd();

            Assert.That(title.GetTitleText(), Is.EqualTo("New Owner"));
        }
    }
}