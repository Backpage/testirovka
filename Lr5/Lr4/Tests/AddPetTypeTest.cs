using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class AddPetTypeTest : BaseTest
    {
        [Test]
        public void AddPetType()
        {
            TabComponent tab = Components.Tab;
            PetTypesPageObject petTypesPage = Pages.PetTypesPage;
            tab.PetTypesOpen();
            petTypesPage.Add("cat");
            Helper.Wait();

            Assert.That(petTypesPage.GetName(), Is.EqualTo("cat"));
        }
    }
}