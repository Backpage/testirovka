using NUnit.Framework;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class OpenOwnersPageTest : BaseTest
    {
        [Test]
        public void OpenOwnersPage()
        {
            TabComponent tab = Components.Tab;
            TitleComponent title = Components.Title;
            tab.OwnersOpenAll();

            Assert.That(title.GetTitleText(), Is.EqualTo("Owners"));
        }
    }
}