﻿using OpenQA.Selenium;

namespace Lr5
{
    class Helper
    {
        public static void ClickAndSendKeys(IWebElement el, string text)
        {
            el.Click();
            el.SendKeys(text);
        }

        public static void ClickAndClearAndSendKeys(IWebElement el, string text)
        {
            el.Click();
            el.Clear();
            el.SendKeys(text);
        }

        public static void Wait(int timeout = 3000)
        {
            Thread.Sleep(timeout);
        }
    }
}
