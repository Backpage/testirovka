﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class OwnersPageObject : BasePageObject
    {
        public OwnersPageObject(IWebDriver driver) : base(driver) { }

        private By OwnerTab = By.CssSelector(".ownerTab");
        private By OwnerAllTab = By.CssSelector(".open li:nth-child(1) > a");
        private By OwnerTitle = By.CssSelector("h2");
        private By OwnerAddTab = By.CssSelector(".open li:nth-child(2) span:nth-child(2)");
        private By OwnerLink = By.CssSelector("tr:last-child .ownerFullName > a");
        private By OwnerEdit = By.CssSelector(".editOwner");
        private By OwnerBack = By.CssSelector(".goBack");
        private By OwnerFirstName = By.Id("firstName");
        private By OwnerLastName = By.Id("lastName");
        private By OwnerAddress = By.Id("address");
        private By OwnerCity = By.Id("city");
        private By OwnerTelephone = By.Id("telephone");
        private By OwnerAdd = By.CssSelector(".addOwner");
        private By OwnerUpdate = By.CssSelector(".updateOwner");

        public void OpenAll()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(OwnerAllTab).Click();
        }

        public string GetTitleText()
        {
            return driver.FindElement(OwnerTitle).Text;
        }

        public void OpenAdd()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(OwnerAddTab).Click();
        }

        public void OpenInfo()
        {
            driver.FindElement(OwnerLink).Click();
        }

        public void OpenEdit()
        {
            driver.FindElement(OwnerEdit).Click();
        }

        public void Add(string firstName, string lastName, string address, string city, string telephone)
        {
            driver.FindElement(OwnerFirstName).Click();
            driver.FindElement(OwnerFirstName).SendKeys(firstName);
            driver.FindElement(OwnerLastName).Click();
            driver.FindElement(OwnerLastName).SendKeys(lastName);
            driver.FindElement(OwnerAddress).Click();
            driver.FindElement(OwnerAddress).SendKeys(address);
            driver.FindElement(OwnerCity).Click();
            driver.FindElement(OwnerCity).SendKeys(city);
            driver.FindElement(OwnerTelephone).Click();
            driver.FindElement(OwnerTelephone).SendKeys(telephone);   
            driver.FindElement(OwnerAdd).Click();
        }

        public string GetName()
        {
            return driver.FindElement(OwnerLink).Text;
        }

        public void Edit(string firstName)
        {
            driver.FindElement(OwnerFirstName).Click();
            driver.FindElement(OwnerFirstName).Clear();
            driver.FindElement(OwnerFirstName).SendKeys(firstName);
            driver.FindElement(OwnerUpdate).Click();
        }

        public void Back()
        {
            driver.FindElement(OwnerBack).Click();
        }
    }
}
