﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        public PetTypesPageObject(IWebDriver driver) : base(driver) { }

        private By PetTypeTab = By.CssSelector("li:nth-child(4) span:nth-child(2)");
        private By PetTypeTitle = By.CssSelector("h2");
        private By PetTypeAdd = By.CssSelector(".addPet");
        private By PetTypeName = By.Id("name");
        private By PetTypeSave = By.CssSelector(".saveType");
        private By PetTypeNameValue = By.CssSelector("tr:last-child input");
        private By PetTypeEdit = By.CssSelector("tr:last-child .editPet");
        private By PetTypeUpdate = By.CssSelector(".updatePetType");
        private By PetTypeDelete = By.CssSelector("tr:last-child .deletePet");

        public void Open()
        {
            driver.FindElement(PetTypeTab).Click(); 
        }

        public string GetTitleText()
        {
            return driver.FindElement(PetTypeTitle).Text;
        }

        public void Add(string name)
        {
            driver.FindElement(PetTypeAdd).Click();
            driver.FindElement(PetTypeName).Click();
            driver.FindElement(PetTypeName).SendKeys(name);
            driver.FindElement(PetTypeSave).Click();
        }

        public string GetName()
        {
            return driver.FindElement(PetTypeNameValue).GetAttribute("value");
        }

        public void Edit(string name)
        {
            driver.FindElement(PetTypeEdit).Click();
            driver.FindElement(PetTypeName).Click();
            driver.FindElement(PetTypeName).Clear();
            driver.FindElement(PetTypeName).SendKeys(name);
            driver.FindElement(PetTypeUpdate).Click();
        }

        public void Delete()
        {
            driver.FindElement(PetTypeDelete).Click();
        }
    }
}
