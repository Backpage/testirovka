using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class AddOwnerTest : BaseTest
    {
        [Test]
        public void AddOwner()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenAdd();
            ownersPage.Add("John", "Doe", "Peremohy str", "Zhytomyr", "0935678352");
            Wait();

            Assert.That(ownersPage.GetName(), Is.EqualTo("John Doe"));
        }
    }
}