﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
namespace Lr4
{
    public abstract class BaseTest
    {
        const string URL = "https://client.sana-commerce.dev";
        const int SCREEN_WIDTH = 1382;
        const int SCREEN_HEIGHT = 744;

        protected IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl(URL);
            driver.Manage().Window.Size = new System.Drawing.Size(SCREEN_WIDTH, SCREEN_HEIGHT);
        }

        [TearDown]
        protected void TearDown()
        {
            driver.Quit();
        }

        public void Wait(int timeout = 3000)
        {
            Thread.Sleep(timeout);
        }
    }
}
