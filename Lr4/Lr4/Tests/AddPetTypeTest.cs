using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class AddPetTypeTest : BaseTest
    {
        [Test]
        public void AddPetType()
        {
            PetTypesPageObject petTypesPage = new PetTypesPageObject(driver);
            petTypesPage.Open();
            petTypesPage.Add("cat");
            Wait();

            Assert.That(petTypesPage.GetName(), Is.EqualTo("cat"));
        }
    }
}