using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class EditOwnerTest : BaseTest
    {
        [Test]
        public void EditOwner()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenAll();
            Wait();
            ownersPage.OpenInfo();
            ownersPage.OpenEdit();
            ownersPage.Edit("Alex");
            Wait();
            ownersPage.Back();
            Wait();

            Assert.That(ownersPage.GetName(), Is.EqualTo("Alex Doe"));
        }
    }
}