using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class OpenPetTypesPageTest : BaseTest
    {
        [Test]
        public void OpenPetTypesPage()
        {
            PetTypesPageObject petTypesPage = new PetTypesPageObject(driver);
            petTypesPage.Open();
            Wait();

            Assert.That(petTypesPage.GetTitleText(), Is.EqualTo("Pet Types"));
        }
    }
}