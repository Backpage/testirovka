using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class EditPetTypeTest : BaseTest
    {
        [Test]
        public void EditPetType()
        {
            PetTypesPageObject petTypesPage = new PetTypesPageObject(driver);
            petTypesPage.Open();
            Wait();
            petTypesPage.Edit("dog");
            Wait();

            Assert.That(petTypesPage.GetName(), Is.EqualTo("dog"));
        }
    }
}