using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class OpenOwnerAddPageTest : BaseTest
    {
        [Test]
        public void OpenOwnerAddPage()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenAdd();

            Assert.That(ownersPage.GetTitleText(), Is.EqualTo("New Owner"));
        }
    }
}