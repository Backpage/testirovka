using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class OpenOwnersPageTest : BaseTest
    {
        [Test]
        public void OpenOwnersPage()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenAll();

            Assert.That(ownersPage.GetTitleText(), Is.EqualTo("Owners"));
        }
    }
}