using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class OpenOwnerInformationPageTest : BaseTest
    {
        [Test]
        public void OpenOwnerInformationPage()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenAll();
            Wait();
            ownersPage.OpenInfo();

            Assert.That(ownersPage.GetTitleText(), Is.EqualTo("Owner Information"));
        }
    }
}