using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class OpenOwnerEditPageTest : BaseTest
    {
        [Test]
        public void OpenOwnerEditPage()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenAll();
            Wait();
            ownersPage.OpenInfo();
            Wait();
            ownersPage.OpenEdit();

            Assert.That(ownersPage.GetTitleText(), Is.EqualTo("Edit Owner"));
        }
    }
}